﻿using Microsoft.EntityFrameworkCore;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Context
{
    public class DatabaseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<ContactRequest> ContactRequests { get; set; }
        public DbSet<Orders> Orderss { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Reviews> Reviews { get; set; }
    }
}
