﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Verteilte_Systeme.Models
{
    [Table("reviews", Schema = "public")]
    public class Reviews
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }
        public int Rating { get; set; }
        public string Username { get; set; }
        public string Text { get; set; }
    }
}
