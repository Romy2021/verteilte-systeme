﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Verteilte_Systeme.Models
{
    [Table("ContactRequest", Schema = "public")]
    public class ContactRequest
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ID")]
        [Key]
        public int ID { get; set; }
        public string CustomerNummer { get; set; }
        public string Subject { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
    }
}
