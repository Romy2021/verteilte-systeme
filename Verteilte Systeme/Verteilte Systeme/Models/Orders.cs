﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Verteilte_Systeme.Models
{
    [Table("Orders", Schema = "public")]
    public class Orders
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }
        public string Purchaser { get; set; }
        public string Product { get; set; }
        public int Quantity { get; set; }
        public int TotalCosts{ get; set; }
    }
}
