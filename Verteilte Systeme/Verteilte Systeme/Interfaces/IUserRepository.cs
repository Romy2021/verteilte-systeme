﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Interfaces
{
    public interface IUserRepository
    {
        List<User> GetUsers();
        User FindUser(int userid);
        User AddUser(User user);
        bool DeleteUser(int userid);
        User UpdateUser(User user);
    }
}
