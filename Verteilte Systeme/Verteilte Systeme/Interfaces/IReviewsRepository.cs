﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Interfaces
{
    public interface IReviewsRepository
    {
        List<Reviews> GetReviews();
        Reviews FindReviews(int rating);
        Reviews AddReviews(Reviews reviews);
        Reviews UpdatedReviews(Reviews reviews);
        bool RevomeReviews(int reviewsid);
    }
}
