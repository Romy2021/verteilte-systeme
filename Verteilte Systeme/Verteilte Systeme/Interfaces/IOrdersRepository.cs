﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Interfaces
{
    public interface IOrdersRepository
    {
        List<Orders> GetOrders();
        Orders FindOrders(string product);
        Orders AddOrders(Orders orders);
        Orders UpdatedOrders(Orders orders);
        bool DeleteOrders(int ordersid);
    }
}
