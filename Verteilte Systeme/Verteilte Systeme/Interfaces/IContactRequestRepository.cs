﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Interfaces
{
    public interface IContactRequestRepository
    {
        List<ContactRequest> GetContacts();
        ContactRequest FindCategory(string category);
        ContactRequest AddContact(ContactRequest contact);
        ContactRequest UpdatedContact(ContactRequest contact);
        bool DeleteContact(string customernummer);
    }
}
