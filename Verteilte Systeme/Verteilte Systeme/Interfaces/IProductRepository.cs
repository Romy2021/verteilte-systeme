﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Interfaces
{
    public interface IProductRepository
    {
        List<Product> GetProducts();
        Product FindProduct(int productid);
        Product AddProduct(Product product);
        bool DeleteProduct(int productid);
        Product UpdateProduct(Product product);
    }
}
