﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Context;
using Verteilte_Systeme.Helper;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Database
{
    public class ProductRepository : IProductRepository
    {
        private ContextConfig contextConfig;
        public ProductRepository(string connection)
        {
            contextConfig = new ContextConfig
            {
                ConnectionString = connection,
            };

            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Database.EnsureCreated();
            }
        }

        public Product AddProduct(Product product)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Products.Add(product);
                context.SaveChanges();
                return product;
            }
        }

        public bool DeleteProduct(int productid)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                var product = context.Products.Find(productid);

                context.Products.Remove(product);
                var num = context.SaveChanges();

                return num > 0;
            }
        }

        public Product FindProduct(int productid)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                return context.Products.Find(productid);
            }
        }

        public List<Product> GetProducts()
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                return context.Products.ToList();
            }
        }

        public Product UpdateProduct(Product product)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Products.Update(product);
                context.SaveChanges();
                return product;
            }
        }
    }
}
