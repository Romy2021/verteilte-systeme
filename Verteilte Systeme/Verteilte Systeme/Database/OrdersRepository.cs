﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Context;
using Verteilte_Systeme.Helper;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Database
{
    public class OrdersRepository : IOrdersRepository
    {
        private ContextConfig contextConfig;

        public OrdersRepository(string connection)
        {
            contextConfig = new ContextConfig
            {
                ConnectionString = connection,
            };

            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Database.EnsureCreated();
            }
        }

        public Orders AddOrders(Orders orders)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Orderss.Add(orders);
                context.SaveChanges();

                return orders;
            }
        }

        public bool DeleteOrders(int ordersid)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                var order = context.Orderss.Find(ordersid);

                if (order != null)
                {
                    context.Remove(order);
                    var num = context.SaveChanges();

                    return num > 0;
                }
                return false;
            }
        }

        public Orders FindOrders(string product)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                return context.Orderss.Where(x => x.Product.Equals(product)).FirstOrDefault();
            }
        }

        public List<Orders> GetOrders()
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                return context.Orderss.ToList();
            }
        }

        public Orders UpdatedOrders(Orders orders)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Orderss.Update(orders);
                context.SaveChanges();

                return orders;
            }
        }
    }
}
