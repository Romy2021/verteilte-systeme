﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Context;
using Verteilte_Systeme.Helper;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Database
{
    public class UserRepository : IUserRepository
    {
        private ContextConfig contextConfig;

        public UserRepository(string connection)
        {
            contextConfig = new ContextConfig
            {
                ConnectionString = connection,
            };

            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Database.EnsureCreated();
            }
        }

        public List<User> GetUsers()
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                return context.Users.ToList();
            }
        }

        public User FindUser(int userid)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                User user = context.Users.Find(userid);

                return user;
            }
        }

        public User AddUser(User user)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Users.Add(user);
                context.SaveChanges();

                return user;
            }
        }
        
        public User UpdateUser(User user)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Users.Update(user);
                context.SaveChanges();

                return user;
            }
        }

        public bool DeleteUser(int userid)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                User user = context.Users.Find(userid);

                context.Users.Remove(user);
                var num = context.SaveChanges();

                return num > 0;
            }
        }
    }
}
