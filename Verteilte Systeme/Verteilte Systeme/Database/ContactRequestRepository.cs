﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Context;
using Verteilte_Systeme.Helper;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Database
{
    public class ContactRequestRepository : IContactRequestRepository
    {
        private ContextConfig contextConfig;

        public ContactRequestRepository(string connection)
        {
            contextConfig = new ContextConfig
            {
                ConnectionString = connection,
            };

            using(var context = ContextFactory.GetContext(contextConfig))
            {
                context.Database.EnsureCreated();
            }
        }

        public ContactRequest AddContact(ContactRequest contact)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.ContactRequests.Add(contact);
                context.SaveChanges();

                return contact;
            }
        }

        public bool DeleteContact(string customernummer)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                var contact = context.ContactRequests.Where(x => x.CustomerNummer.Equals(customernummer)).FirstOrDefault();

                if (contact != null)
                {
                    context.ContactRequests.Remove(contact);
                    var num = context.SaveChanges();

                    return num > 0;
                }
                return false;
            }
        }

        public ContactRequest FindCategory(string category)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                return context.ContactRequests.Where(x => x.Category.Equals(category)).FirstOrDefault();
            }
        }

        public List<ContactRequest> GetContacts()
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                return context.ContactRequests.ToList();
            }
        }

        public ContactRequest UpdatedContact(ContactRequest contact)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.ContactRequests.Update(contact);
                var num = context.SaveChanges();

                if (num > 0)
                {
                    return contact;
                }
                return null;

            }
        }
    }
}
