﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Context;
using Verteilte_Systeme.Helper;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Database
{
    public class ReviewsRepository : IReviewsRepository
    {
        private ContextConfig contextConfig;

        public ReviewsRepository(string connection)
        {
            contextConfig = new ContextConfig
            {
                ConnectionString = connection,
            };

            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Database.EnsureCreated();
            }
        }

        public Reviews AddReviews(Reviews reviews)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Reviews.Add(reviews);
                context.SaveChanges();

                return reviews;
            }
        }

        public Reviews FindReviews(int rating)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                return context.Reviews.Where(x => x.Rating == rating).FirstOrDefault();
            }
        }

        public List<Reviews> GetReviews()
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                return context.Reviews.ToList();
            }
        }

        public Reviews UpdatedReviews(Reviews reviews)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                context.Reviews.Update(reviews);
                context.SaveChanges();

                return reviews;
            }
        }

        public bool RevomeReviews(int reviewsid)
        {
            using (var context = ContextFactory.GetContext(contextConfig))
            {
                var review = context.Reviews.Find(reviewsid);

                if (review != null)
                {
                    context.Reviews.Update(review);
                    var num = context.SaveChanges();

                    return num > 0;
                }
                return false;
            }
        }
    }
}
