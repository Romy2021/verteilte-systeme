﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Context;

namespace Verteilte_Systeme.Helper
{
    public class PostgresSqlContext : DatabaseContext
    {
        private string connection;

        public PostgresSqlContext(string con)
        {
            connection = con;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            optionBuilder.UseNpgsql(connection);
            base.OnConfiguring(optionBuilder);
        }
    }
}
