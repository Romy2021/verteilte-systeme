﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verteilte_Systeme.Context;

namespace Verteilte_Systeme.Helper
{
    public class ContextFactory
    {
        public static DatabaseContext GetContext(ContextConfig contextConfig)
        {
            return new PostgresSqlContext(contextConfig.ConnectionString);
        }
    }
}
