﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Verteilte_Systeme.Helper
{
    public class ContextConfig
    {
        public string ConnectionString { get; set; }
    }
}
