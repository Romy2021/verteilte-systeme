﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Verteilte_Systeme.Context;
using Verteilte_Systeme.Database;
using Verteilte_Systeme.Helper;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Controllers
{
    [ApiController]
    [Route("api/product")]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class ControllerProduct : Controller
    {
        private readonly IProductRepository productRepository;
        private readonly AppSettings appSettings;

        public ControllerProduct(IOptions<AppSettings> options)
        {
            this.appSettings = options.Value;
            productRepository = new ProductRepository(appSettings.Settings.ConnectionString);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var products = productRepository.GetProducts();

            if (products != null)
            {
                return Ok(products);
            }

            return BadRequest();
        }

        [HttpGet("{productid}")]
        public IActionResult Get(int productid)
        {
            var product = productRepository.FindProduct(productid);

            if (product != null)
            {
                return Ok(product);
            }

            return BadRequest();
        }

        [HttpPost]
        public IActionResult Post(Product product)
        {
            var currentProduct = productRepository.AddProduct(product);

            if (currentProduct != null)
            {
                return Ok(currentProduct);
            }

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Put(Product product)
        {
            var currentProduct = productRepository.UpdateProduct(product);

            if (currentProduct != null)
            {
                return Ok(currentProduct);
            }

            return BadRequest();
        }

        [HttpDelete]
        public IActionResult Delete(int productid)
        {
            var deleted = productRepository.DeleteProduct(productid);

            if (deleted)
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
