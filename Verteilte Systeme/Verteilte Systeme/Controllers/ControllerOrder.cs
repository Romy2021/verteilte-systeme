﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Verteilte_Systeme.Context;
using Verteilte_Systeme.Database;
using Verteilte_Systeme.Helper;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Controllers
{
    [ApiController]
    [Route("api/orders")]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class ControllerOrder : Controller
    {
        private readonly IOrdersRepository ordersRepository;
        private readonly AppSettings appSettings;

        public ControllerOrder(IOptions<AppSettings> options)
        {
            this.appSettings = options.Value;
            ordersRepository = new OrdersRepository(appSettings.Settings.ConnectionString);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var orders = ordersRepository.GetOrders();

            if (orders != null)
            {
                return Ok(orders);
            }

            return BadRequest();
        }

        [HttpGet("{product}")]
        public IActionResult Get(string product)
        {
            var order = ordersRepository.FindOrders(product);

            if (order != null)
            {
                return Ok(order);
            }

            return BadRequest();
        }

        [HttpPost]
        public IActionResult Post(Orders orders)
        {
            var order = ordersRepository.AddOrders(orders);

            if (order != null)
            {
                return Ok(order);
            }

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Put(Orders orders)
        {
            var order = ordersRepository.UpdatedOrders(orders);

            if (order != null)
            {
                return Ok(order);
            }

            return BadRequest();
        }

        [HttpDelete]
        public IActionResult Delete(int ordersid)
        {
            var delete = ordersRepository.DeleteOrders(ordersid);

            if (delete)
            {
                return Ok(true);
            }

            return BadRequest();
        }
    }
}
