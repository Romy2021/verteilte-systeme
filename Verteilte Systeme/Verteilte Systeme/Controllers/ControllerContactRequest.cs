﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Verteilte_Systeme.Context;
using Verteilte_Systeme.Database;
using Verteilte_Systeme.Helper;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Controllers
{
    [ApiController]
    [Route("api/contactrequest")]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class ControllerContactRequest : Controller
    {
        private readonly IContactRequestRepository contactRequestRepository;
        private readonly AppSettings appSettings;

        public ControllerContactRequest(IOptions<AppSettings> options)
        {
            this.appSettings = options.Value;
            contactRequestRepository = new ContactRequestRepository(appSettings.Settings.ConnectionString);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var contactRequests = contactRequestRepository.GetContacts();

            if (contactRequests != null)
            {
                return Ok(contactRequests);
            }

            return BadRequest();
        }

        [HttpGet("{category}")]
        public IActionResult Get(string category)
        {
            var contactRequest = contactRequestRepository.FindCategory(category);

            if (contactRequest != null)
            {
                return Ok(contactRequest);
            }

            return BadRequest();
        }

        [HttpPost]
        public IActionResult Post(ContactRequest contactRequest)
        {
            var currentcontactRequest = contactRequestRepository.AddContact(contactRequest);

            if (currentcontactRequest != null)
            {
                return Ok(currentcontactRequest);
            }

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Put(ContactRequest contactRequest)
        {
            var currentcontact = contactRequestRepository.UpdatedContact(contactRequest);

            if (currentcontact != null)
            {
                return Ok(currentcontact);
            }

            return BadRequest();
        }

        [HttpDelete]
        public IActionResult Delete(string customernummer)
        {
            var deleted = contactRequestRepository.DeleteContact(customernummer);

            if (deleted)
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
