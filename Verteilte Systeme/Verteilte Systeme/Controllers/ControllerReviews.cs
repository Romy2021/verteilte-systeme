﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Verteilte_Systeme.Context;
using Verteilte_Systeme.Database;
using Verteilte_Systeme.Helper;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Controllers
{
    [ApiController]
    [Route("api/reviews")]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class ControllerReviews : Controller
    {
        private readonly IReviewsRepository reviewsRepository;
        private readonly AppSettings appSettings;

        public ControllerReviews(IOptions<AppSettings> options)
        {
            this.appSettings = options.Value;
            reviewsRepository = new ReviewsRepository(appSettings.Settings.ConnectionString);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var reviews = reviewsRepository.GetReviews();

            if (reviews != null)
            {
                return Ok(reviews);
            }

            return BadRequest();
        }

        [HttpGet("{rating}")]
        public IActionResult Get(int rating)
        {
            var review = reviewsRepository.FindReviews(rating);

            if (review != null)
            {
                return Ok(review);
            }

            return BadRequest();
        }

        [HttpPost]
        public IActionResult Post(Reviews reviews)
        {
            var review = reviewsRepository.AddReviews(reviews);

            if (review != null)
            {
                return Ok(review);
            }

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Put(Reviews reviews)
        {
            var review = reviewsRepository.UpdatedReviews(reviews);

            if (review != null)
            {
                return Ok(review);
            }

            return BadRequest();
        }

        [HttpDelete]
        public IActionResult Delete(int reviewid)
        {
            var delete = reviewsRepository.RevomeReviews(reviewid);

            if (delete)
            {
                return Ok(true);
            }

            return BadRequest();
        }
    }
}
