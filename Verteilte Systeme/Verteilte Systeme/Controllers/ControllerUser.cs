﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Net.Mime;
using Verteilte_Systeme.Context;
using Verteilte_Systeme.Database;
using Verteilte_Systeme.Helper;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme.Controllers
{
    [ApiController]
    [Route("api/users")]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class ControllerUser : Controller
    {
        private readonly IUserRepository userRepository;
        private readonly AppSettings appSettings;

        public ControllerUser(IOptions<AppSettings> options)
        {
            this.appSettings = options.Value;
            userRepository = new UserRepository(appSettings.Settings.ConnectionString);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var users = userRepository.GetUsers();

            return Ok(users);
        }

        [HttpGet("{userid}")]
        public IActionResult Get(int userid)
        {
            var users = userRepository.FindUser(userid);

            return Ok(users);
        }

        [HttpPost]
        public IActionResult Post(User user)
        {
            var users = userRepository.AddUser(user);

            return Ok(users);
        }

        [HttpPut]
        public IActionResult Put(User user)
        {
            var updated = userRepository.UpdateUser(user);

            return Ok(updated);
        }

        [HttpDelete]
        public IActionResult Delete(int userid)
        {
            var deleted = userRepository.DeleteUser(userid);

            return Ok(deleted);
        }
    }
}
