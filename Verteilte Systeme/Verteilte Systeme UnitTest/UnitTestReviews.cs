﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme_UnitTest
{
    [TestClass]
    public class UnitTestReviews
    {
        private IReviewsRepository reviewsRepository;

        public UnitTestReviews()
        {
            List<Reviews> reviews = new List<Reviews>()
            {
                new Reviews() { ID = 1 , Rating = 5, Username = "kaan97", Text = "Das Produkt ist super gut!" },
                new Reviews() { ID = 2 , Rating = 5, Username = "clara45", Text = "Die Bambuszahnbürste hat mein Leben verändert" },
                new Reviews() { ID = 3 , Rating = 4, Username = "thomas11", Text = "Die Zahnbürste macht einen sehr soliden Eindruck" },
                new Reviews() { ID = 4 , Rating = 2, Username = "hater22", Text = "Sehr schlecht verarbeitet!" },
            };

            Mock<IReviewsRepository> mockRepo = new Mock<IReviewsRepository>();

            mockRepo.Setup(m => m.GetReviews()).Returns(reviews);
            mockRepo.Setup(m => m.FindReviews(It.IsAny<int>())).Returns((int i) => reviews.Where(x => x.Rating == i).Single());
            mockRepo.Setup(m => m.AddReviews(It.IsAny<Reviews>())).Returns(
            (Reviews target) =>
            {
                if (target.ID.Equals(default(int)))
                {
                    target.ID = reviews.Count() + 1;
                    target.Rating = 1;
                    target.Username = "test";
                    target.Text = "test";

                    reviews.Add(target);

                    return target;
                }
                else
                {
                    var original = reviews.Where(m => m.ID == target.ID).Single();
                    if (original == null)
                    {
                        return null;
                    }
                    original.Rating = target.Rating;
                    original.Username = target.Username;
                    original.Text = target.Text;

                    return original;
                }
            });

            this.reviewsRepository = mockRepo.Object;
        }

        [TestMethod]
        public void Get_Anzahl_der_Reviews()
        {
            var reviews = reviewsRepository.GetReviews();

            Assert.AreEqual(4, reviews.Count);
        }

        [TestMethod]
        public void Find_ein_Reviews_where_ratung_gleich_2()
        {
            var rating = 2;

            var reviews = reviewsRepository.FindReviews(rating);

            Assert.AreEqual(4, reviews.ID);
            Assert.AreNotEqual("kaan97", reviews.Username);
        }

        [TestMethod]
        public void Add_neuer_Reviews()
        {
            Reviews reviews = new Reviews() { Rating = 10, Username = "tasse56", Text = "Sehr schlecht qualität!" };

            Reviews currentReviews = reviewsRepository.AddReviews(reviews);

            Assert.AreEqual(5, currentReviews.ID);
        }

        [TestMethod]
        public void Update_Reviews()
        {
            Reviews reviews = new Reviews() { Rating = 10, Username = "tasse56", Text = "Sehr schlecht qualität!" };

            reviews.Rating = 20;

            Reviews currentReviews = reviewsRepository.UpdatedReviews(reviews);

            Assert.AreEqual(20, reviews.Rating);
            Assert.AreNotEqual(10, reviews.Rating);
        }
    }
}
