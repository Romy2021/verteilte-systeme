﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme_UnitTest
{
    [TestClass]
    public class UnitTestProduct
    {
        public IProductRepository productRepository;

        public UnitTestProduct()
        {
            List<Product> products = new List<Product>()
            {
                new Product() { ID = 1 , Name = "Bambuszahnbürste", Price = 10, Brand = "LifePanda", Since = 2020, Url = "../img/Produkt_1.jpg" },
                new Product() { ID = 2 , Name = "Papiermüllbeutel", Price =10, Brand = "LifePanda", Since = 2020, Url = "../img/Produkt_3.jpg" },
                new Product() { ID = 3 , Name = "Tasse", Price = 5, Brand = "LifePanda", Since = 2010, Url = "../img/Produkt_5.jpg" },
                new Product() { ID = 4 , Name = "Orange", Price = 20, Brand = "LifePanda", Since = 2000, Url = "../img/Produkt_7.jpg" },
            };

            Mock<IProductRepository> mockRepo = new Mock<IProductRepository>();

            mockRepo.Setup(m => m.GetProducts()).Returns(products);
            mockRepo.Setup(m => m.FindProduct(It.IsAny<int>())).Returns((int i) => products.Where(x => x.ID == i).Single());
            mockRepo.Setup(m => m.AddProduct(It.IsAny<Product>())).Returns(
            (Product target) =>
            {
                if (target.ID.Equals(default(int)))
                {
                    target.ID = products.Count() + 1;
                    target.Name = "Test";
                    target.Price = 1;
                    target.Brand = "Test";
                    target.Since = 0000;
                    target.Url = "test";
                    products.Add(target);

                    return target;
                }
                else
                {
                    var original = products.Where(m => m.ID == target.ID).Single();
                    if (original == null)
                    {
                        return null;
                    }
                    original.Name = target.Name;
                    original.Price = target.Price;
                    original.Brand = target.Brand;
                    original.Since = target.Since;
                    original.Url = target.Url;

                    return original;
                }
            });

            this.productRepository = mockRepo.Object;
        }

        [TestMethod]
        public void Get_Anzahl_der_Users()
        {
            var users = productRepository.GetProducts();

            Assert.AreEqual(4, users.Count);
        }

        [TestMethod]
        public void Find_ein_User_where_productid_gleich_3()
        {
            var user = productRepository.FindProduct(3);

            Assert.AreEqual("Tasse", user.Name);
            Assert.AreNotEqual("Orange", user.Name);
        }

        [TestMethod]
        public void Add_neuer_Product()
        {
            Product product = new Product() { Name = "Orange", Price = 20, Brand = "LifePanda", Since = 2000, Url = "../img/Produkt_7.jpg" };

            var addProduct = productRepository.AddProduct(product);

            Assert.AreEqual(5, addProduct.ID);
        }

        [TestMethod]
        public void Update_Product()
        {
            Product product = new Product() { Name = "Orange", Price = 20, Brand = "LifePanda", Since = 2000, Url = "../img/Produkt_7.jpg" };
            
            product.Name = "Tomate";

            var updateProduct = productRepository.UpdateProduct(product);

            Assert.AreEqual("Tomate", product.Name);
            Assert.AreNotEqual("Orange", product.Name);
        }
    }
}
