﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Verteilte_Systeme.Context;
using Verteilte_Systeme.Database;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme_UnitTest
{
    [TestClass]
    public class UnitTestUser
    {
        public IUserRepository userRepository { get; }

        public UnitTestUser()
        {
            List<User> users = new List<User>()
            {
                new User() { ID = 1 , Email = "k.erbay@me.com", Firstname = "Kaan", Lastname = "Erbay", Password = "111", Username = "kaan97" },
                new User() { ID = 2 , Email = "papa@test.de", Firstname = "Papa", Lastname = "Mann", Password = "222", Username = "Ppapa" },
                new User() { ID = 3 , Email = "mama@test.de", Firstname = "Mama", Lastname = "Frau", Password = "333", Username = "Mmama" },
                new User() { ID = 4 , Email = "alfred@test.de", Firstname = "Alfred", Lastname = "Wilfred", Password = "444", Username = "Aalfred" },
                new User() { ID = 5 , Email = "paul@test.de", Firstname = "Hartmann", Lastname = "paul", Password = "555", Username = "Hhartmann" },
                new User() { ID = 6 , Email = "theordor@test.de", Firstname = "Mustermann", Lastname = "alex", Password = "666", Username = "Mmustermann" },
            };

            Mock<IUserRepository> mockRepo = new Mock<IUserRepository>();

            mockRepo.Setup(m => m.GetUsers()).Returns(users);
            mockRepo.Setup(m => m.FindUser(It.IsAny<int>())).Returns((int i) => users.Where(x => x.ID == i).Single());
            mockRepo.Setup(m => m.AddUser(It.IsAny<User>())).Returns(
            (User target) =>
            {
                if (target.ID.Equals(default(int)))
                {
                    target.ID = users.Count() + 1;
                    target.Username = "Rromual";
                    target.Firstname = "Kongang";
                    target.Lastname = "Romual";
                    target.Email = "Romual@Romual.de";
                    target.Password = "123445";
                    users.Add(target);

                    return target;
                }
                else
                {
                    var original = users.Where(m => m.ID == target.ID).Single();
                    if (original == null)
                    {
                        return null;
                    }
                    original.Username = target.Username;
                    original.Firstname = target.Firstname;
                    original.Lastname = target.Lastname;
                    original.Email = target.Email;
                    original.Password = target.Password;

                    return original;
                }
            });

            this.userRepository = mockRepo.Object;
        }

        [TestMethod]
        public void Get_Anzahl_der_Users()
        {
            var users = userRepository.GetUsers();

            Assert.AreEqual(6, users.Count);
        }

        [TestMethod]
        public void Find_ein_User_where_id_gleich_3()
        {
            var user = userRepository.FindUser(3);

            Assert.AreEqual("Mama", user.Firstname);
            Assert.AreNotEqual("Papa", user.Firstname);
        }

        [TestMethod]
        public void Add_neuer_user()
        {
            User user = new User() { Email = "Romual@Romual.de", Firstname = "Kongang", Lastname = "Romual", Password = "1234", Username = "Rrmonual" };

            var addUser = userRepository.AddUser(user);

            Assert.AreEqual(7, addUser.ID);
        }

        [TestMethod]
        public void Update_user()
        {
            User user = new User() { Email = "Romual@Romual.de", Firstname = "Kongang", Lastname = "Peter", Password = "1234", Username = "Rrmonual" };

            var updateUser = userRepository.AddUser(user);
            updateUser.Lastname = "Peter";

            Assert.AreEqual("Peter", updateUser.Lastname);
            Assert.AreNotEqual("Romual", updateUser.Lastname);
        }
    }
}
