﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme_UnitTest
{
    public class UnitTestOrder
    {
        private IOrdersRepository ordersRepository;

        public UnitTestOrder()
        {
            List<Orders> orders = new List<Orders>()
            {
                new Orders() { ID = 1 , Purchaser = "kaan97", Product = "Nachhaltiges-Einkaufsnetz", Quantity = 2, TotalCosts = 38,},
                new Orders() { ID = 2 , Purchaser = "clara45", Product = "Bambuszahnbürste", Quantity = 2, TotalCosts = 20 },
                new Orders() { ID = 3 , Purchaser = "thomas11", Product = "Bambuszahnbürste", Quantity = 1, TotalCosts = 10 },
                new Orders() { ID = 4 , Purchaser = "hater22", Product = "Bambuszahnbürste", Quantity = 1, TotalCosts = 10 },
            };

            Mock<IOrdersRepository> mockRepo = new Mock<IOrdersRepository>();

            mockRepo.Setup(m => m.GetOrders()).Returns(orders);
            mockRepo.Setup(m => m.FindOrders(It.IsAny<string>())).Returns((string i) => orders.Where(x => x.Product == i).Single());
            mockRepo.Setup(m => m.AddOrders(It.IsAny<Orders>())).Returns(
            (Orders target) =>
            {
                if (target.ID.Equals(default(int)))
                {
                    target.ID = orders.Count() + 1;
                    target.Purchaser = "test";
                    target.Product = "test";
                    target.Quantity = 1;
                    target.TotalCosts = 1;

                    orders.Add(target);

                    return target;
                }
                else
                {
                    var original = orders.Where(m => m.ID == target.ID).Single();
                    if (original == null)
                    {
                        return null;
                    }
                    original.Purchaser = target.Purchaser;
                    original.Product = target.Product;
                    original.Quantity = target.Quantity;
                    original.TotalCosts = target.TotalCosts;

                    return original;
                }
            });

            this.ordersRepository = mockRepo.Object;
        }

        [TestMethod]
        public void Get_Anzahl_der_Orders()
        {
            var orderss = ordersRepository.GetOrders();

            Assert.AreEqual(4, orderss.Count);
        }

        [TestMethod]
        public void Find_ein_order_where_product_gleich_Nachhaltiges_Einkaufsnetz()
        {
            var product = "Nachhaltiges-Einkaufsnetz";

            var order = ordersRepository.FindOrders(product);

            Assert.AreEqual(1, order.ID);
            Assert.AreNotEqual("hater22", order.Purchaser);
        }

        [TestMethod]
        public void Add_neuer_Order()
        {
            Orders orders = new Orders() { Purchaser = "small22", Product = "Bambuszahnbürste", Quantity = 10, TotalCosts = 100 };

            Orders cuurentOrder = ordersRepository.AddOrders(orders);

            Assert.AreEqual(5, cuurentOrder.ID);
        }

        [TestMethod]
        public void Update_Order()
        {
            Orders orders = new Orders() { Purchaser = "small22", Product = "Bambuszahnbürste", Quantity = 10, TotalCosts = 100 };

            orders.Purchaser = "kapputt45";

            Orders cuurentOrder = ordersRepository.AddOrders(orders);

            Assert.AreEqual("kapputt45", cuurentOrder.Purchaser);
            Assert.AreNotEqual("small22", cuurentOrder.Purchaser);
        }
    }
}
