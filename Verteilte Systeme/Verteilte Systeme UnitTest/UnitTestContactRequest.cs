﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verteilte_Systeme.Interfaces;
using Verteilte_Systeme.Models;

namespace Verteilte_Systeme_UnitTest
{
    [TestClass]
    public class UnitTestContactRequest
    {
        private IContactRequestRepository contactRequestRepository;

        public UnitTestContactRequest()
        {
            List<ContactRequest> contactRequests = new List<ContactRequest>()
            {
                new ContactRequest() { ContactRequestID = 1 , CustomerNummer = "1234567p", Subject = "Eure Seite ist Super!", Firstname = "Kaan", Lastname = "Erbay", Email = "k.erbay@me.com", Category = "andere Anliegen", Description = "Ich habe schon häufiger von Greeny bestellt und wollte ein positives Feedback hinterlassen." },
                new ContactRequest() { ContactRequestID = 2 , CustomerNummer = "7654321p", Subject = "Zahnbürste ist kaputt geliefert worden", Firstname = "Clara", Lastname = "Balkenhoff", Email = "clara@gmail.com", Category = "Bambuszahnbürsten", Description = "Heute kamen die Bambuszahnbürsten an. Leider musste ich feststellen, dass Sie einen großen Riss haben." },
            };

            Mock<IContactRequestRepository> mockRepo = new Mock<IContactRequestRepository>();

            mockRepo.Setup(m => m.GetContacts()).Returns(contactRequests);
            mockRepo.Setup(m => m.FindCategory(It.IsAny<string>())).Returns((string i) => contactRequests.Where(x => x.Category == i).Single());
            mockRepo.Setup(m => m.AddContact(It.IsAny<ContactRequest>())).Returns(
            (ContactRequest target) =>
            {
                if (target.ContactRequestID.Equals(default(int)))
                {
                    target.ContactRequestID = contactRequests.Count() + 1;
                    target.CustomerNummer = "test";
                    target.Subject = "test";
                    target.Firstname = "test";
                    target.Lastname = "test";
                    target.Email = "test";
                    target.Category = "test";
                    target.Description = "test";
                    contactRequests.Add(target);

                    return target;
                }
                else
                {
                    var original = contactRequests.Where(m => m.ContactRequestID == target.ContactRequestID).Single();
                    if (original == null)
                    {
                        return null;
                    }
                    original.CustomerNummer = target.CustomerNummer;
                    original.Subject = target.Subject;
                    original.Firstname = target.Firstname;
                    original.Lastname = target.Lastname;
                    original.Email = target.Email;
                    original.Category = target.Category;
                    original.Description = target.Description;

                    return original;
                }
            });

            this.contactRequestRepository = mockRepo.Object;
        }

        [TestMethod]
        public void Get_Anzahl_der_ContactRequests()
        {
            var users = contactRequestRepository.GetContacts();

            Assert.AreEqual(2, users.Count);
        }

        [TestMethod]
        public void Find_ein_ContactRequest_where_categorie()
        {
            string categorie = "andere Anliegen";

            var contactRequest = contactRequestRepository.FindCategory(categorie);

            Assert.AreEqual(1, contactRequest.ContactRequestID);
            Assert.AreNotEqual("Clara", contactRequest.Firstname);
        }

        [TestMethod]
        public void Add_neuer_ContactRequest()
        {
            ContactRequest contact = new ContactRequest() { CustomerNummer = "76543234p", Subject = "Tasse ist kaputt geliefert worden", Firstname = "Marcel", Lastname = "Wolfgang", Email = "marcel@gmail.com", Category = "Essen", Description = "Heute kamen die Bambuszahnbürsten an. Leider musste ich feststellen, dass Sie einen großen Riss haben." };

            ContactRequest contactRequest = contactRequestRepository.AddContact(contact);

            Assert.AreEqual(3, contactRequest.ContactRequestID);
        }

        [TestMethod]
        public void Update_ContactRequest()
        {
            ContactRequest contact = new ContactRequest() { CustomerNummer = "76543234p", Subject = "Tasse ist kaputt geliefert worden", Firstname = "Marcel", Lastname = "Wolfgang", Email = "marcel@gmail.com", Category = "Essen", Description = "Heute kamen die Bambuszahnbürsten an. Leider musste ich feststellen, dass Sie einen großen Riss haben." };

            contact.CustomerNummer = "76543234a";

            ContactRequest contactRequest = contactRequestRepository.UpdatedContact(contact);

            Assert.AreEqual("76543234a", contact.CustomerNummer);
        }
    }
}
